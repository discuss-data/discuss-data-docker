#let conf(
  title: none,
  pdftitle: none,
  numbering: none,
  page_numbers: "1",
  both: false,
  doc,
) = {
  set page(
    paper: "a4",
    footer: context [
      #set align(right)
      #set text(10pt)
      #counter(page).display(
        page_numbers,
        both: both,
      )
    ]
  )

  set heading(numbering: numbering)
  set par(justify: true, leading: 0.52em,)
  set document(title: [Discuss Data #pdftitle])

  set text(
      font: "Liberation Sans",
      size: 12pt
    )

  let bigger(term) = {
    text(size: 1.2em, term)
  }

  align(
    image("logo-primary.svg", width: 50%)
  )

  bigger[Archiving, sharing and discussing research data on Eastern Europe, South Caucasus and Central Asia]

  line(length: 100%)
  heading(
    bookmarked: false, numbering: none, bigger(strong(title))
  )
  doc
}

#let vspace=12pt
