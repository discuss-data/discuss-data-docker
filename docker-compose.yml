---
  name: discussdata

  volumes:
    production_django_data: {}
    production_django_media: {}
    production_django_static: {}
    production_postgres_data: {}
    production_postgres_data_backups: {}
    production_elasticsearch_data: {}

  networks:
    elasticsearch: {}
    elasticsearch_metrics: {}
    ingress: {}
    postgres: {}
    postgres_metrics: {}
    redis: {}
    redis_metrics: {}
    pdf_service: {}


  services:
    ingress:
      image: docker.io/nginx:1.26
      volumes:
        - ./nginx/nginx.conf.template:/etc/nginx/templates/nginx.conf.template:ro
        # disable default configuration
        - ./nginx/default.conf:/etc/nginx/conf.d/default.conf
        - production_django_data:/app/data/:ro
        - production_django_media:/app/media/:ro
        - production_django_static:/app/staticfiles/:ro
      env_file:
        - ./.envs/.production/.ingress
      restart: always
      networks:
        - ingress
      ports:
        - "5000:80"

    pdf_service:
      build:
        context: .
        dockerfile: ./pdf_service/Dockerfile
      image: docker.gitlab.gwdg.de/discuss-data/discuss-data/pdf_service:latest
      environment:
        - PDF_SERVICE_ENVIRONMENT=production
      ports:
        - "5150:5150"
      restart: always
      networks:
        - pdf_service

    django:
      # overwrite user in dockerfile since this does not have a fixed UID/GID
      user: "100:101"
      image: docker.gitlab.gwdg.de/discuss-data/discuss-data:${ENVIRONMENT:-staging}
      depends_on:
        - postgres
        - redis
        - elasticsearch
      volumes:
        - production_django_data:/app/data
        - production_django_media:/app/media
        - production_django_static:/app/staticfiles
      env_file:
        - ./.envs/.production/.django
        - ./.envs/.production/.postgres
      command: /start
      restart: always
      networks:
        - ingress
        - elasticsearch
        - postgres
        - redis
        - pdf_service

    cms:
      user: "100:101"
      image: docker.gitlab.gwdg.de/discuss-data/discuss-data:${ENVIRONMENT:-staging}
      depends_on:
        - postgres
      volumes:
        - production_django_media:/app/media
        - production_django_static:/app/staticfiles
      env_file:
        - ./.envs/.production/.cms
        - ./.envs/.production/.postgres
      command: /start
      restart: always
      networks:
        - ingress
        - elasticsearch
        - postgres
        - redis

    qcluster:
      user: "100:101"
      image: docker.gitlab.gwdg.de/discuss-data/discuss-data:${ENVIRONMENT:-staging}
      depends_on:
        - postgres
        - redis
      volumes:
        - production_django_data:/app/data
      env_file:
        - ./.envs/.production/.django
        - ./.envs/.production/.postgres
      command: /app/manage.py qcluster
      restart: always
      networks:
        - ingress
        - elasticsearch
        - postgres
        - redis

    postgres:
      build:
        context: ./postgres
      image: discuss_data_production_postgres
      volumes:
        - production_postgres_data:/var/lib/postgresql/data
        - production_postgres_data_backups:/backups
      env_file:
        - ./.envs/.production/.postgres
      restart: always
      networks:
        - postgres
        - postgres_metrics

    elasticsearch:
      attach: false
      image: docker.elastic.co/elasticsearch/elasticsearch:7.5.2
      environment:
        - discovery.type=single-node
      volumes:
        - production_elasticsearch_data:/usr/share/elasticsearch/data
      restart: always
      networks:
        - elasticsearch
        - elasticsearch_metrics

    redis:
      attach: false
      image: docker.io/redis:7.2
      restart: always
      networks:
        - redis
        - redis_metrics

    redis_metrics:
      attach: false
      image: oliver006/redis_exporter
      environment:
        - REDIS_ADDR=redis://redis:6379
      restart: always
      networks:
        - redis_metrics

    elasticsearch_metrics:
      attach: false
      image: vinted/elasticsearch_exporter
      command:
       - '--elasticsearch_url=http://elasticsearch:9200'
      restart: always
      networks:
        - elasticsearch_metrics

    postgres_metrics:
      attach: false
      image: wrouesnel/postgres_exporter
      env_file:
        - ./.envs/.production/.postgres
      restart: always
      networks:
        - postgres_metrics

    prometheus:
      attach: false
      build:
        context: ./prometheus
      image: prometheus
      restart: always
      networks:
        - postgres_metrics
        - elasticsearch_metrics
        - redis_metrics
      ports:
        - "9090:9090"
