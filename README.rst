Discuss Data Docker
===================

This project provides a setup for the ``production`` environment of the Discuss Data project.

Setup
-----
The ``docker-compose.yml`` describes the composition of the ``production`` stack.


Settings
--------

Settings are read from environment configuration files located in ``.envs/``.
Bear in mind that no settings for the production environment are provided.
You could easily create your own environment settings from the documentation on
`Configuring the Environment`_.

.. _`Configuring the Environment`: https://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html#configuring-the-environment

Basic Commands
--------------

Backup
------

run backup:
::

   docker-compose exec postgres backup

show backups:
::

   docker-compose exec postgres backups

check backup mount:
::

   docker volume inspect discussdata_production_postgres_data_backups

current backup mount:
::

   /var/lib/docker/volumes/discussdata_production_postgres_data_backups/_data

restore (stop services that may write to db):
::

  docker-compose stop django cms qcluster
  docker-compose exec postgres restore ...
  docker-compose start django cms qcluster



Badges
------

.. image:: https://img.shields.io/badge/commitizen-friendly-brightgreen.svg
     :target: http://commitizen.github.io/cz-cli/
     :alt: Commitizen friendly

.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
     :target: https://www.gnu.org/licenses/gpl-3.0
     :alt: License: GPL v3
